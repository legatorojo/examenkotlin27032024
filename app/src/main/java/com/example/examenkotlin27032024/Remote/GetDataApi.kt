package com.example.examenkotlin27032024.Remote

import com.example.examenkotlin27032024.Model.GetCharactersResponse
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface GetDataApi {


    @GET
    suspend fun getDataCharacters(
            @Url url: String
    ): Response<GetCharactersResponse>

}