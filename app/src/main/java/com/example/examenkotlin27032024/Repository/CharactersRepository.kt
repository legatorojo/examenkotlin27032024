package com.example.examenkotlin27032024.Repository

import android.util.Log
import com.example.examenkotlin27032024.Remote.GetDataApi
import kotlinx.coroutines.flow.flow

class CharactersRepository {


    private val remote =
        RetrofitInstance.getRetrofitInstance().create(GetDataApi::class.java)


    suspend fun GetDataCharacters(
    ) = flow {
        try {
            val response = remote.getDataCharacters(
                url = "https://rickandmortyapi.com/api/character/?page=1",
            )

            emit(response.body())
        } catch (e: Exception) {
            Log.e("ERROR", e.stackTraceToString())
        }
    }

}