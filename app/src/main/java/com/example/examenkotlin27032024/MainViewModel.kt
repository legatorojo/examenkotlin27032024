package com.example.examenkotlin27032024

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.examenkotlin27032024.Model.GetCharactersResponse
import com.example.examenkotlin27032024.Repository.CharactersRepository
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {

    private val repository = CharactersRepository()

    val getCharactersResponse = MutableLiveData<GetCharactersResponse>()


    fun getCharacters(
    ) = viewModelScope.launch {
        repository.GetDataCharacters(
        ).collectLatest { repositoryResponse ->

            getCharactersResponse.postValue(repositoryResponse)

        }
    }

}