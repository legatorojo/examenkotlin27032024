package com.example.examenkotlin27032024

import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import coil.compose.AsyncImage
import com.example.examenkotlin27032024.Model.CharacterData
import com.example.examenkotlin27032024.ui.theme.ExamenKotlin27032024Theme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewModel: MainViewModel by viewModels()

        viewModel.getCharacters()


        viewModel.getCharactersResponse.observe(this, Observer { data->

            Log.d("Examen", "Datos:" + data.results.size);

            setContent {
                ExamenKotlin27032024Theme {

                    drawCharacters(data.results)

                }
            }

        })


    }
}


/*Funcion para dibular todas las lineas con los personajes*/
@Composable
fun drawCharacters(characters: List<CharacterData> )
{
    LazyColumn{

        items(characters){character->

            drawCharacterRow(data = character)
        }

    }
}

/*Funcion para dibular una sola linea para un personaje*/
@Composable
fun drawCharacterRow(data: CharacterData)
{
    Row(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxSize()
    )
    {
        drawImagen(data = data)
        dataCharacter(data = data)
    }
}

/*Funcion para dibular la imagen del perosnaje*/
@Composable
fun drawImagen(data: CharacterData)
{
    AsyncImage(
        model = data.image,
        contentDescription = "Image of " + data.name,
        modifier = Modifier
            .height(110.dp)
            .clip(CircleShape)
    )
}

/*Funcion para dibular la información del personaje*/
@Composable
fun dataCharacter(data: CharacterData) {

    var detalleVisible by remember { mutableStateOf(false) }

    Column(
        modifier = Modifier.padding(16.dp)
    ) {
        Row(
            horizontalArrangement = Arrangement.Start
        ) {
            Text(
                text = "${data.name}",
                style = MaterialTheme.typography.titleLarge
            )
        }

        Row(
            horizontalArrangement = Arrangement.Center
        ) {
            if(detalleVisible)
            {
                drawDetalles(data = data)
            }
        }

        Row(
            horizontalArrangement = Arrangement.Center
        ) {
            Button(onClick = {

                detalleVisible = !detalleVisible

            }) {
                Text(text = if(!detalleVisible)"Ver detalle" else "Ocultar detalle")
            }
        }


    }

}

/*Funcion para dibular los detalles del personaje*/
@Composable
fun drawDetalles(data: CharacterData)
{
    Column(
        modifier = Modifier.padding(8.dp)
    ) {
        drawLineDetalle(atributo = "Status: ", valor = data.status)
        drawLineDetalle(atributo = "Species: ", valor = data.species)
        drawLineDetalle(atributo = "Type: ", valor = data.type)
        drawLineDetalle(atributo = "Gender: ", valor = data.gender)
        drawLineDetalle(atributo = "Origin: ", valor = data.origin.name)
        drawLineDetalle(atributo = "Location: ", valor = data.location.name)
    }

}


/*Funcion para dibular una linea de detalle del personaje*/
@Composable
fun drawLineDetalle(atributo: String, valor: String)
{

    Row(
        horizontalArrangement = Arrangement.Start
    ) {
        Text(
            text = "$atributo",
            style = MaterialTheme.typography.labelLarge
        )
        Text(
            text = "$valor",
            style = MaterialTheme.typography.labelSmall
        )
    }


}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    ExamenKotlin27032024Theme {
        //Greeting("Android")
    }
}