package com.example.examenkotlin27032024.Model

data class GetCharactersResponse (

    val results: List<CharacterData> = listOf()

)