package com.example.examenkotlin27032024.Model


data class CharacterData(

    var id: Int,
    var name: String,
    var image: String,
    var status: String,
    var species: String,
    var type:  String,
    var gender: String,
    var origin: SiteCharacter,
    var location: SiteCharacter

)